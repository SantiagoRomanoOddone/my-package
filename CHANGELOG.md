# CHANGELOG



## v0.2.1 (2024-03-24)

### Fix

* fix(deploy): add deploy pipeline ([`58b79f4`](https://gitlab.com/SantiagoRomanoOddone/my-package/-/commit/58b79f4a4403cb9032824acdd9bf9208e0198f19))

### Unknown

* Merge branch &#39;main&#39; of gitlab.com:SantiagoRomanoOddone/my-package into main ([`e50ed3d`](https://gitlab.com/SantiagoRomanoOddone/my-package/-/commit/e50ed3dde66d3b8f5b960f704fc39dc744e5e6b3))


## v0.2.0 (2024-03-24)

### Feature

* feat(addition): add addition feature ([`dc53634`](https://gitlab.com/SantiagoRomanoOddone/my-package/-/commit/dc536341365dbebc0b75089806b8e63137f664c1))

### Unknown

* Merge branch &#39;main&#39; of gitlab.com:SantiagoRomanoOddone/my-package into main ([`5efa2da`](https://gitlab.com/SantiagoRomanoOddone/my-package/-/commit/5efa2da8c4ff8406c8efa76e154fa522c0c2addb))


## v0.1.0 (2024-03-24)

### Feature

* feat(sr): add semantic release cd pipeline ([`1a8da5b`](https://gitlab.com/SantiagoRomanoOddone/my-package/-/commit/1a8da5be75af8d75eb380488c7283eb43545bb65))

### Unknown

* add semantic_release config ([`b9b9744`](https://gitlab.com/SantiagoRomanoOddone/my-package/-/commit/b9b974470e69441421c04ab6b62fdd5ebdf4c9fb))

* Add pyproject.toml to init poetry ([`3abf3f2`](https://gitlab.com/SantiagoRomanoOddone/my-package/-/commit/3abf3f2d03c505b7559b320b06a822fa3772b818))

* Update .gitlab-ci.yml file ([`5378f04`](https://gitlab.com/SantiagoRomanoOddone/my-package/-/commit/5378f04b8773335b998da7ca9c512884cc02af48))

* Add first pipeline ([`6e709a7`](https://gitlab.com/SantiagoRomanoOddone/my-package/-/commit/6e709a7fa5fff07fdbd352fc95719b138ca00fa8))

* Initial commit ([`213bc42`](https://gitlab.com/SantiagoRomanoOddone/my-package/-/commit/213bc42d38fa5ad558c4b9bd0c8acd6d90887456))
